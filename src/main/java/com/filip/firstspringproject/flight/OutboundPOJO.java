package com.filip.firstspringproject.flight;

public class OutboundPOJO {
	
	private Airport departureAirport;
	private Airport arrivalAirport;
	private String departureDate;
	private String arrivalDate;
	private PricePOJO price;
	
	public OutboundPOJO () {}
	
	public OutboundPOJO(Airport departureAirport, Airport arrivalAirport, String departureDate, String arrivalDate,
			PricePOJO price) {
		super();
		this.departureAirport = departureAirport;
		this.arrivalAirport = arrivalAirport;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.price = price;
	}

	public Airport getDepartureAirport() {
		return departureAirport;
	}

	public void setDepartureAirport(Airport departureAirport) {
		this.departureAirport = departureAirport;
	}

	public Airport getArrivalAirport() {
		return arrivalAirport;
	}

	public void setArrivalAirport(Airport arrivalAirport) {
		this.arrivalAirport = arrivalAirport;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public PricePOJO getPrice() {
		return price;
	}

	public void setPrice(PricePOJO price) {
		this.price = price;
	}
}

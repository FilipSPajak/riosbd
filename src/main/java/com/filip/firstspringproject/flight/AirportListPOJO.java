package com.filip.firstspringproject.flight;

import java.util.List;

public class AirportListPOJO {
	
	private List<Airport> airportList;
	
	public AirportListPOJO () {}

	public List<Airport> getAirportList() {
		return airportList;
	}

	public void setAirportList(List<Airport> airportList) {
		this.airportList = airportList;
	}
}

package com.filip.firstspringproject.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationService {

	@Autowired
	ReservationRepository reservationRepository;
	
	public void createNewReservation(Reservation reservation) {
		reservationRepository.save(reservation);
	}
	
}

package com.filip.firstspringproject.flight;

import java.util.List;

public class ResponsePOJO {
	
	private int total;
	private List<FarePOJO> fares;
	
	public ResponsePOJO() {
		
	}

	public ResponsePOJO(List<FarePOJO> fares, int total) {
		super();
		this.fares = fares;
		this.total = total;
	}

	public List<FarePOJO> getFares() {
		return fares;
	}

	public void setFares(List<FarePOJO> fares) {
		this.fares = fares;
	}
	
	public int getTotal() {
		return this.total;
	}
	
	public void setTotal(int total) {
		this.total = total;
	}
}

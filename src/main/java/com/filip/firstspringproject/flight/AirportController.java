package com.filip.firstspringproject.flight;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AirportController {

	@Autowired
	private AirportService airportService;

	@RequestMapping("/airports")
	public List<Airport> printAirports() {
		return airportService.getAllAirports();
	}
	
	@RequestMapping("/airports/{id}")
	public Airport printSingleAirport(@PathVariable String id) {
		return airportService.getSingleAirport(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/airports")
	public void insertNewAirport(@RequestBody Airport airport) {
		airportService.addNewAirport(airport);
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/airports/{name}")
	public int getAirportIdByName(@PathVariable String name) {
		return airportService.getAirportByName(name).getId();
	}	
	
	@RequestMapping(method = RequestMethod.PUT, value="/airports/{id}")
	public void updateAirport(@RequestBody Airport airport, @PathVariable String id) {
		airportService.updateAirport(airport);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/airports/{id}")
	public void deleteAirport(@PathVariable String id) {
		airportService.deleteAirport(id);
	}
}

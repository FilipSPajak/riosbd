package com.filip.firstspringproject.flight;

import java.util.List;

public class AirportResponsePOJO {
	
	private List<Airport> airports;
	
	public AirportResponsePOJO() {}

	public AirportResponsePOJO(List<Airport> airports) {
		super();
		this.airports = airports;
	}

	public List<Airport> getAirports() {
		return airports;
	}

	public void setAirports(List<Airport> airports) {
		this.airports = airports;
	}
	
	

}

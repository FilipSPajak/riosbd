package com.filip.firstspringproject.flight;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RyanairRequestSender {
	
	private final static String baseURL = "https://api.ryanair.com/farefinder/3/oneWayFares?&departureAirportIataCode=WRO&language=en&limit=16&market=en-gb&offset=0&outboundDepartureDateFrom=2017-12-22&outboundDepartureDateTo=2017-12-22&priceValueTo=150";
	private final static String RYANAIR_AIRPORT_URL = "https://desktopapps.ryanair.com/en-gb/res/stations";
	private final static String ALL_AIRPORTS_URL = "https://api.ryanair.com/aggregate/3/common?embedded=airports&market=en-gb";

	
	public ResponsePOJO sendRequest(String outboundAirport, String dateFrom, String dateTo, int maxPriceValue) {
		
		RestTemplate restTemplate = new RestTemplate();
		
		String finalURL = RyanairRequestBuilder.getRequestURL(outboundAirport, dateFrom, dateTo, maxPriceValue);
		
		ResponseEntity<ResponsePOJO> response = restTemplate.exchange(finalURL, HttpMethod.GET, null, ResponsePOJO.class);
		return response.getBody();
	}
	
	public String getRyanairAirports() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> response = restTemplate.exchange(RYANAIR_AIRPORT_URL, HttpMethod.GET, null, String.class);
		return response.getBody();
	}
	
	public AirportResponsePOJO sendAllAirportsRequest() {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<AirportResponsePOJO> response = restTemplate.exchange(ALL_AIRPORTS_URL, HttpMethod.GET, null, AirportResponsePOJO.class);
		return response.getBody();
	}

}

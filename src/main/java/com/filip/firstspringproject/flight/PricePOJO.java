package com.filip.firstspringproject.flight;

public class PricePOJO {
	
	double value;
	String currencyCode;
	
	public PricePOJO () {}
	
	public PricePOJO(double value, String currencyCode) {
		super();
		this.value = value;
		this.currencyCode = currencyCode;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
}

package com.filip.firstspringproject.flight;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="flights")
public class Flight {
	
	@Id
	@Column(name="id", nullable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	// @ManyToOne(fetch=FetchType.LAZY)
	private int departureAirportId;
	
	// @ManyToOne(fetch=FetchType.LAZY)
	private int arrivalAirportId;
	
	@Column(name="departure_date")
	private String departureDate;
	
	@Column(name="arrival_date")
	private String arrivalDate;
	
	@Column(name="price_value")
	private double price;
	
	@Column(name="price_code")
	private String priceCode;
	
	private int airlineId;
	
	private int airplaneId;
	
	public Flight() {
		
	}

	public Flight(int id, int departureAirportId, int arrivalAirportId, String departureDate, String arrivalDate,
			double price, String priceCode, int airlineId, int airplaneId) {
		super();
		this.id = id;
		this.departureAirportId = departureAirportId;
		this.arrivalAirportId = arrivalAirportId;
		this.departureDate = departureDate;
		this.arrivalDate = arrivalDate;
		this.price = price;
		this.priceCode = priceCode;
		this.airlineId = airlineId;
		this.airplaneId = airplaneId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDepartureAirportId() {
		return departureAirportId;
	}

	public void setDepartureAirportId(int departureAirportId) {
		this.departureAirportId = departureAirportId;
	}

	public int getArrivalAirportId() {
		return arrivalAirportId;
	}

	public void setArrivalAirportId(int arrivalAirportId) {
		this.arrivalAirportId = arrivalAirportId;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getPriceCode() {
		return priceCode;
	}

	public void setPriceCode(String priceCode) {
		this.priceCode = priceCode;
	}

	public int getAirlineId() {
		return airlineId;
	}

	public void setAirlineId(int airlineId) {
		this.airlineId = airlineId;
	}

	public int getAirplaneId() {
		return airplaneId;
	}

	public void setAirplaneId(int airplaneId) {
		this.airplaneId = airplaneId;
	}
	
	

}

package com.filip.firstspringproject.flight;

import java.util.Map;

public class FlightCriteria {
	
	private int departureAirportId;
	private int arrivalAirportId;
	private String departureTime;
	private String arrivalTime;
	
	private String departureDate;
	private String arrivalDate;
	
	public FlightCriteria(Map<String, String> requestParams) {
		this.departureDate = requestParams.get("departureDate");
		this.arrivalDate = requestParams.get("arrivalDate");
	}
	
	public void extractTimeFromDate() {
		if(arrivalDate != null) {
			arrivalTime = arrivalDate + "T00:00:00";
		}
		if(departureDate != null) {
		departureTime = departureDate + "T00:00:00";
		}
	}

	public int getDepartureAirportId() {
		return departureAirportId;
	}

	public void setDepartureAirportId(int departureAirportId) {
		this.departureAirportId = departureAirportId;
	}

	public int getArrivalAirportId() {
		return arrivalAirportId;
	}

	public void setArrivalAirportId(int arrivalAirportId) {
		this.arrivalAirportId = arrivalAirportId;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		extractTimeFromDate();
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureDate() {
		return departureDate;
	}
	
	public String getDepartureLikeDate() {
		return departureDate + '%';
	}
	
	public String getArrivalLikeDate() {
		return arrivalDate + '%';
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalDate() {
		extractTimeFromDate();
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}
	
	
}

package com.filip.firstspringproject.flight;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlightController {
	
	@Autowired
	private FlightService flightService;
	
	@Autowired
	private AirportService airportService;

	@RequestMapping("/flights")
	public List<Flight> printFlights() {
		return flightService.getAllFlights();
	}
	
	@RequestMapping("/flights/{id}")
	public Flight printSingleFlight(@PathVariable int id) {
		String flightId = Integer.toString(id);
		return flightService.getSingleFlight(id);
	}
	
	@RequestMapping("/flights/filter")
	public List<Flight> getFilteredFlights(@RequestParam(required=false) Map<String, String> requestParams) {
		List<Flight> flights;
		
		String departureAirportName = requestParams.get("departureAirport");
		String arrivalAirportName = requestParams.get("arrivalAirport");
		
		Airport dep = airportService.getAirportByName(departureAirportName);
		Airport arr = airportService.getAirportByName(arrivalAirportName);
		
		FlightCriteria flightCriteria = new FlightCriteria(requestParams);
		flightCriteria.setDepartureAirportId(dep.getId());
		flightCriteria.setArrivalAirportId(arr.getId());
		flights = flightService.getMatchingFlights(flightCriteria);
		
		return flights;	
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/flights")
	public void insertNewFlight(@RequestBody Flight flight) {
		flightService.addNewFlight(flight);
	}
	
	@RequestMapping(method = RequestMethod.PUT, value="/flights/{id}")
	public void updateFlight(@RequestBody Flight flight, @PathVariable int id) {
		flightService.updateFlight(id, flight);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value="/flights/{id}")
	public void deleteFlight(@PathVariable int id) {
		flightService.deleteFlight(id);
	}
	
}

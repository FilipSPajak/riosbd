package com.filip.firstspringproject.flight;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ReservationRepository extends CrudRepository<Reservation, String> {

}

package com.filip.firstspringproject.flight;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FarePOJO {
	
	private OutboundPOJO outbound;
	
	public FarePOJO () {}

	public FarePOJO(OutboundPOJO outbound) {
		super();
		this.outbound = outbound;
	}

	public OutboundPOJO getOutbound() {
		return outbound;
	}

	public void setOutbound(OutboundPOJO outbound) {
		this.outbound = outbound;
	}
}

package com.filip.firstspringproject.flight;

public class RyanairRequestBuilder {
	
	public static final String baseURL = "https://api.ryanair.com/farefinder/3/oneWayFares?&departureAirportIataCode=";
	
	public static String getRequestURL(String departureAirportIataCode, String outboundDepartureDateFrom, String outboundDepartureDateTo, int maxPriceValue) {
		String url = baseURL;
		url += departureAirportIataCode;
		url += "&language=en&limit=16&market=en-gb&offset=0&outboundDepartureDateFrom=";
		url += outboundDepartureDateFrom;
		url += "&outboundDepartureDateTo=";
		url += outboundDepartureDateTo;
		url += "&priceValueTo=";
		url += maxPriceValue;
		return url;
	}
}

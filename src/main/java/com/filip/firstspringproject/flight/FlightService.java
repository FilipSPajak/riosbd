package com.filip.firstspringproject.flight;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filip.firstspringproject.flight.FlightRepository;

@Service
public class FlightService {
	
	@Autowired
	private FlightRepository flightRepository;
	
	public List<Flight> getAllFlights() {
		List<Flight> flightsList = new ArrayList<>();
		flightRepository.findAll().forEach(flightsList::add);
		return flightsList;
	}
	
	public Flight getSingleFlight(int id) {
		Flight flight = flightRepository.findOne(id);
		return flight;
	}

	public void addNewFlight(Flight flight) {
		flight.setAirlineId(0);
		flight.setAirplaneId(MathUtil.generateRandomAirplaneId());
		flightRepository.save(flight);
	}

	public void updateFlight(int id, Flight flight) {
		
		flightRepository.update(
				id, 
				flight.getDepartureAirportId(),
				flight.getArrivalAirportId(), 
				flight.getDepartureDate(), 
				flight.getArrivalDate(), 
				flight.getPrice(), 
				flight.getPriceCode(), 
				flight.getAirlineId(), 
				flight.getAirplaneId());
	}

	public void deleteFlight(int id) {
		flightRepository.delete(id);
	}

	public List<Flight> getMatchingFlights(FlightCriteria flightCriteria) {
		return flightRepository.findMatchingFlights(flightCriteria);
	}
	
	
	
}

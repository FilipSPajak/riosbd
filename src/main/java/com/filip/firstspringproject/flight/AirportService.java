package com.filip.firstspringproject.flight;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AirportService {

	@Autowired
	private AirportRepository airportRepository;
	
	public List<Airport> getAllAirports() {
		List<Airport> airportsList = new ArrayList<>();
		airportRepository.findAll().forEach(airportsList::add);
		return airportsList;
	}
	
	public Airport getSingleAirport(String id) {
		Airport airport = airportRepository.findOne(id);
		return airport;
	}
	
	public Airport findByName(Airport sourceAirport) {
		String sourceAirportName = sourceAirport.getName();
		Airport outputAirport = airportRepository.findByName(sourceAirportName);
		if(outputAirport == null) {
			addNewAirport(sourceAirport);
			outputAirport = airportRepository.findByName(sourceAirportName);
		}
		return outputAirport;
	}

	public void addNewAirport(Airport airport) {
		airportRepository.save(airport);
	}

	public void updateAirport(Airport airport) {
		airportRepository.update(airport.getId(), airport.getCountryName(), airport.getIataCode());
	}

	public void deleteAirport(String id) {
		airportRepository.delete(id);
	}

	public Airport getAirportByName(String name) {
		return airportRepository.findByName(name);
	
	}

	
	
}

package com.filip.firstspringproject.flight;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {

	@Autowired
	ReservationService reservationService;
	
	@RequestMapping(method = RequestMethod.POST, value = "/reservation")
	public void addNewReservation(@RequestBody Reservation reservation) {
		reservationService.createNewReservation(reservation);
	}
	
}

package com.filip.firstspringproject.flight;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="airports")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Airport {
	
	@Id
	@Column(name="id", nullable=false, updatable=false)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="name")
	private String name;
	@Column(name="iata_code")
	private String iataCode;
	@Column(name="country_name")
	private String countryName;
//	private String countryCode;
//	private String latitude;
//	private String longitude;
	
//	private String timeZone;
	
//	@JsonIgnore
//	@OneToMany
//	private Set<Flight> flights = new HashSet<>(0);
	
	public Airport() {
		
	}
	
	public Airport(int id, String name, String iataCode, String countryName, String countryCode, String latitude,
		String longitude) {
		super();
		this.id = id;
		this.name = name;
		this.iataCode = iataCode;
		this.countryName = countryName;
//		this.countryCode = countryCode;
//		this.latitude = latitude;
//		this.longitude = longitude;
	}

	


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIataCode() {
		return iataCode;
	}

	public void setIataCode(String iataCode) {
		this.iataCode = iataCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

//	public String getCountryCode() {
//		return countryCode;
//	}
//
//	public void setCountryCode(String countryCode) {
//		this.countryCode = countryCode;
//	}
//
//	public String getLatitude() {
//		return latitude;
//	}
//
//	public void setLatitude(String latitude) {
//		this.latitude = latitude;
//	}
//
//	public String getLongitude() {
//		return longitude;
//	}
//
//	public void setLongitude(String longitude) {
//		this.longitude = longitude;
//	}
//	
//	public String getTimeZone() {
//		return timeZone;
//	}
//
//	public void setTimeZone(String timeZone) {
//		this.timeZone = timeZone;
//	}
	
	
	
}

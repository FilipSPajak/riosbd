package com.filip.firstspringproject.flight;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface AirportRepository extends CrudRepository<Airport, String> {
	
	@Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Airport a SET a.name = ?2, a.iataCode = ?3 WHERE a.id = ?1")
    public void update(int airportId, String name, String iataCode);
	
	public Airport findByName(String name);


}

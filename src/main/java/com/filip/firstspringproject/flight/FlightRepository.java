package com.filip.firstspringproject.flight;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

public interface FlightRepository extends CrudRepository<Flight, Integer> {
	
	@Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE Flight a SET a.departureAirportId = ?2, "
    		+ "a.arrivalAirportId = ?3, "
    		+ "a.departureDate = ?4,"
    		+ "a.arrivalDate = ?5, "
    		+ "a.price = ?6, "
    		+ "a.priceCode = ?7, "
    		+ "a.airlineId = ?8, "
    		+ "a.airplaneId = ?9 WHERE a.id = ?1")
    public void update(int airportId, int depId, int arrId, String depDate, String arrDate, double price, String priceCode, int airlineId, int airplaneId);

	@Query(value = "SELECT * "
			+ "FROM flights " 
			+ "WHERE (:#{[0].getDepartureAirportId()} IS null OR departure_airport_id = :#{[0].getDepartureAirportId()}) "
			+ "AND (:#{[0].getArrivalAirportId()} IS null OR arrival_airport_id = :#{[0].getArrivalAirportId()}) "
			+ "AND (:#{[0].getDepartureTime()} IS null OR departure_date LIKE :#{[0].getDepartureLikeDate()})"
			+ "AND (:#{[0].getArrivalTime()} IS null OR arrival_date LIKE :#{[0].getArrivalLikeDate()})"
			+ "ORDER BY departure_date DESC"
			, nativeQuery = true)
	public List<Flight> findMatchingFlights(FlightCriteria criteria);
}

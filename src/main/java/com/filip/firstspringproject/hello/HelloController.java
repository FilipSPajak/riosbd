package com.filip.firstspringproject.hello;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.filip.firstspringproject.flight.AirportListPOJO;
import com.filip.firstspringproject.flight.AirportResponsePOJO;
import com.filip.firstspringproject.flight.AirportService;
import com.filip.firstspringproject.flight.FarePOJO;
import com.filip.firstspringproject.flight.Flight;
import com.filip.firstspringproject.flight.FlightService;
import com.filip.firstspringproject.flight.MathUtil;
import com.filip.firstspringproject.flight.OutboundPOJO;
import com.filip.firstspringproject.flight.ResponsePOJO;
import com.filip.firstspringproject.flight.RyanairRequestSender;

import com.filip.firstspringproject.flight.Airport;

@RestController
public class HelloController {
	
	private final static String outboundAirport = "WRO";
	private final static String dateFrom = "2018-01-11";
	private final static String dateTo = "2018-01-20";
	private final static int maxPriceValue = 1500;
	
	private final static int RYANAIR_AIRLINE_ID = 0;
	
	@Autowired
	FlightService flightService;
	
	@Autowired
	AirportService airportService;
	
	@RequestMapping("/air")
	public void getAllAirports() {
		RyanairRequestSender sender = new RyanairRequestSender();
		AirportResponsePOJO response = sender.sendAllAirportsRequest();
		List<Airport> allAirports = response.getAirports();
		for(Airport airport : allAirports) {
			airportService.updateAirport(airport);
		}
		
	}

	@RequestMapping("/fetchflights")
	public String sayHi() {
		
		List<Airport> airports = airportService.getAllAirports();
		List<String> airportNames = new ArrayList<>();
		
		for(int i = 0; i < airports.size(); i++) {
			airportNames.add(airports.get(i).getIataCode());
		}
		
		RyanairRequestSender sender = new RyanairRequestSender();
		
		for(int i = 0; i < airportNames.size(); i++) {
					
			ResponsePOJO response = sender.sendRequest(airportNames.get(i), dateFrom, dateTo, maxPriceValue);
					
			if(response.getFares().size() > 0) {
				String name = response.getFares().get(0).getOutbound().getDepartureAirport().getName();
				
				List<FarePOJO> fares = response.getFares();
								
				for(FarePOJO fare : fares) {
					OutboundPOJO outbound = fare.getOutbound();
					Flight flight = new Flight();
					Airport arrivalAirport = outbound.getArrivalAirport();
					Airport departureAirport = outbound.getDepartureAirport();
					
					arrivalAirport = airportService.findByName(arrivalAirport);
					departureAirport = airportService.findByName(departureAirport);
					
					flight.setArrivalAirportId(arrivalAirport.getId());
					flight.setDepartureAirportId(departureAirport.getId());
					flight.setArrivalDate(outbound.getArrivalDate());
					flight.setDepartureDate(outbound.getDepartureDate());
					flight.setPrice(outbound.getPrice().getValue());
					flight.setPriceCode(outbound.getPrice().getCurrencyCode());
					flight.setAirlineId(0);
					flight.setAirplaneId(MathUtil.generateRandomAirplaneId());
					
					//System.out.println(flight.toString());
					
					flightService.addNewFlight(flight);
				}
			}
		}
		
		return "All flights from " + dateFrom + " to " + dateTo + " fetched.";
	}
}

﻿var app = angular.module('flightSearchEngine', []);

app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);

app.controller('Hello', function ($scope, $http) {
	$scope.baseUrl = "http://953d3807.ngrok.io"
	$scope.serverUrl = $scope.baseUrl +  "/flights"
	$scope.url = "https://cdn.pixabay.com/photo/2016/02/19/10/51/clouds-1209444_1280.jpg"
	$scope.flights = [];
	$scope.newFlight = {};


	$scope.showFlights = function () {
		//$scope.searchFlightsByCriteria();
		//$scope.changeDisplay();
		$http.get($scope.serverUrl).
			then(function (response) {
				$scope.allFlights = response.data;
				for(var i = response.data.length - 15; i < response.data.length; i++) {
					$scope.flights.push(response.data[i]);
				}
				$scope.changeDisplay();
			}).catch(function onError(response) {
			});
	};

	$scope.searchFlightsByCriteria = function () {
		console.log('dep date:');
		$http({
			method: "GET",
			url: $scope.serverUrl + "/filter",
			params: {
				departureAirportId: $scope.departureCity,
				arrivalAirportId: $scope.arrivalCity
				//departureDate: '2018-01-15T22:00:00',//$scope.arrivalDatePicked
				//arrivalDate: '2018-01-15T23:50:00'// $scope.arrivalDatePicked
			},
			headers: {'Accept':' application/json', 'Content-Type': "application/json"},
		}).success(function(data, status, headers, config){
			for(var i = 0; i < data.length; i++) {
				$scope.flights.push(data[i]);
				console.log(data[i]);
			}
			$scope.changeDisplay();
		})
	}

	$scope.makeReservation = function (chosenFlightId) {
		$http({
			method:"POST",
			url: $scope.baseUrl + "/reservation",
			data: {
				flightId: chosenFlightId
			}
		}).success(function(){

		})
	}

	$scope.getDepartureAirportNameById = function (flight) {
		$http({
			method: "GET",
			url: $scope.baseUrl + "/airports/" + flight.departureAirportId
		}).success(function(data) {
			$scope.airportName = data;
		})
	}

	$scope.getArrivalAirportNameById = function () {

	}
	
	$scope.getDepartureAirportByName = function () {
		$http({
			method: "GET",
			url: $scope.baseUrl + "/airports/" + $scope.newFlight.newDepartureAirport
		}).success(function(data) {
			$scope.newFlight.newDepartureAirportId = data;
		})
	}

	$scope.getArrivalAirportByName = function () {
		$http({
			method: "GET",
			url: $scope.baseUrl + "/airports/" + $scope.newFlight.newArrivalAirport
		}).success(function(data) {
			$scope.newFlight.newArrivalAirportId = data;
		})
	}

	$scope.makeNewFlight = function () {
		$scope.getArrivalAirportByName();
		$scope.getDepartureAirportByName();
		$http({
			method:"POST",
			url: $scope.serverUrl,
			data: {
				departureAirportId : $scope.newFlight.newDepartureAirportId,
				arrivalAirportId : $scope.newFlight.newArrivalAirportId,
				departureDate : $scope.newFlight.newDepartureDate,
				arrivalDate : $scope.newFlight.newArrivalDate,
				price : $scope.newFlight.newPriceValue,
				priceCode : $scope.newFlight.newPriceCode
			}
		}).success(function(){
			console.log($scope.newFlight.newArrivalAirportId)
		}).error(function(){
			console.log('e' + $scope.newFlight.newArrivalAirportId);
		})
	}

	$scope.firstTwentyRecords = function (allFlights) {
		for (i = 0; i < 10; i++)
		{
			$scope.addFlight(allFlights[i].departureDate, allFlights[i].arrivalDate, allFlights[i].price)
		}
	}
		
	$scope.changeDisplay = function () {
		$scope.display = !$scope.display;
	 }

	$scope.deleteFlight = function (index) {
        delete $scope.flights[index];
	}

	$scope.addFlight = function (departureDate, arrivalDate, price) {
		$scope.flights.push({ DepartureDate: departureDate, ArrivalDate: arrivalDate, Price: price })
    }
});


app.filter("myCurrency", function () {
    return function (input) {
        return input + "PLN";
    }
})

app.filter("mySpliter", function () {
    return function (input) {
        return input.split('').join(' ');
    }
})

app.myFilter = function (item) { 
    return item.priceCode == 'PLN'; 
};

$(document).on('focus', '.datepicker',function(){
	$(this).datepicker({
		todayHighlight:true,
		format:'yyyy-mm-dd',
		autoclose:true
	})
});

﻿var app = angular.module('demo', []);

app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);
	
app.controller('Hello', function ($scope, $http) {
	$scope.baseUrl = "http://f0e7d211.ngrok.io"
	$scope.serverUrl = $scope.baseUrl +  "/flights"
	$scope.url = "https://cdn.pixabay.com/photo/2016/02/19/10/51/clouds-1209444_1280.jpg"
	$scope.flights = [];
	$scope.newFlight = {};


	$scope.showFlights = function () {
		$http.get($scope.serverUrl).
			then(function (response) {
				$scope.allFlights = response.data;
				//$scope.addFlight($scope.flight.departureDate, $scope.flight.arrivalDate, $scope.flight.price)
				//$scope.findRequestedFligths($scope.allFlights);
				$scope.firstTwentyRecords();
				$scope.changeDisplay();
			}).catch(function onError(response) {
				$scope.dupa = response.status;
			});
	};

	$scope.searchFlightsByCriteria = function () {
		console.log('dep date:');
		$http({
			method: "GET",
			url: "http://localhost:8082/flights/filter",
			params: {
				departureAirport: $scope.departureCity,
				arrivalAirport: $scope.arrivalCity,
				departureDate: '2018-01-15',
				arrivalDate: '2018-01-15'
			},
			headers: {'Accept':' application/json', 'Content-Type': "application/json"},
		}).success(function(data, status, headers, config){
			for(var i = 0; i < data.length; i++) {
				$scope.flights.push(data[i]);
				console.log(data[i]);
			}
		})
	}

	$scope.makeReservation = function (chosenFlightId) {
		$http({
			method:"POST",
			url: $scope.baseUrl + "/reservation",
			data: {
				flightId: chosenFlightId
			}
		}).success(function(){

		})
	}

	$scope.getDepartureAirportByName = function () {
		$http({
			method: "GET",
			url: $scope.baseUrl + "/airports/" + $scope.newFlight.newDepartureAirport
		}).success(function(data) {
			$scope.newFlight.newDepartureAirportId = data;
		})
	}

	$scope.getArrivalAirportByName = function () {
		$http({
			method: "GET",
			url: $scope.baseUrl + "/airports/" + $scope.newFlight.newArrivalAirport
		}).success(function(data) {
			$scope.newFlight.newArrivalAirportId = data;
		})
	}

	$scope.makeNewFlight = function () {
		$scope.getArrivalAirportByName();
		$scope.getDepartureAirportByName();
		$http({
			method:"POST",
			url: $scope.serverUrl,
			data: {
				departureAirportId : $scope.newFlight.newDepartureAirportId,
				arrivalAirportId : $scope.newFlight.newArrivalAirportId,
				departureDate : $scope.newFlight.newDepartureDate,
				arrivalDate : $scope.newFlight.newArrivalDate,
				price : $scope.newFlight.newPriceValue,
				priceCode : $scope.newFlight.newPriceCode
			}
		}).success(function(){
			console.log($scope.newFlight.newArrivalAirportId)
		}).error(function(){
			console.log('e' + $scope.newFlight.newArrivalAirportId);
		})
	}

	$scope.firstTwentyRecords = function (allFlights) {
		for (i = 0; i < 10; i++)
		{
			$scope.addFlight(flight.departureDate, flight.arrivalDate, flight.price)
		}
	}

	$scope.findRequestedFligths = function (allFlights) {
		var chosenDepartureDate = document.getElementById("departureDate").value;
		var chosenArrivalDate = document.getElementById("departureDate").value;

		for (flight in allFlights) {
			var dd = flight.departureDate;
			var ad = flight.arrivalDate;
			if (dd == chosenDepartureDate
				&& ad == chosenArrivalDate) {
				$scope.addFlight(flight.departureDate, flight.arrivalDate, flight.price)
			}
		}
	}

	$scope.createNewFlight = function () {
		var data = $.param({
			"departureAirportId" : parseInt($scope.newDepartureAirportId),
			"arrivalAirportId" : parseInt($scope.newArrivalAirportId),
			"departureDate" : $scope.newDepartureDate,
			"arrivalDate" : $scope.newArrivalDate,
			"price" : parseInt($scope.newPriceValue),
			"priceCode" : $scope.newPriceCode
		});

		console.log(data);
		var config = {
			headers: {
				'Content-Type': 'application/json'
			}
		}
		//config.setRequestProperty("Content-Type", "application/json; charset=utf8")

		$http.post($scope.serverUrl, data, config)
			.success(function (data, status, headers, config) {
				$scope.PostDataResponse = data;
				alert("Zapisano w bazie");
			})
			.error(function (data, status, header, config) {
				$scope.ResponseDetails = "Data: " + data +
					"<hr />status: " + status +
					"<hr />headers: " + header +
					"<hr />config: " + config;
				alert($scope.ResponseDetails);
			});
	};
	
	 $scope.addFlight222 = function(){
            var encodeString = "id=79&content=qwerty";
            $http({
				method: "POST",
				url: $scope.serverUrl,
                data: encodeString,
                headers: {'Accept':' text/plain', 'Content-Type': "application/x-www-form-urlencoded"}
            }).success(function(data, status, headers, config){
                console.log(data);
            }).error(function(data, status, headers, config){
                console.log(data);
                console.log(status);
                console.log(headers);
                console.log(config);
                console.log("Error submit form");
            });
        }
		
	$scope.changeDisplay = function () {
		$scope.display = !$scope.display;
		$scope.searchFlightsByCriteria();
	 }

	$scope.deleteFlight = function (index) {
        delete $scope.flights[index];
	}

	$scope.addFlight = function (departureDate, arrivalDate, price) {
		$scope.flights.push({ DepartureDate: departureDate, ArrivalDate: arrivalDate, Price: price })
        alert("Znaleziono");
    }
});


app.filter("myCurrency", function () {
    return function (input) {
        return input + "PLN";
    }
})

app.filter("mySpliter", function () {
    return function (input) {
        return input.split('').join(' ');
    }
})

$(document).on('focus', '.datepicker',function(){
	$(this).datepicker({
		todayHighlight:true,
		format:'yyyy-mm-dd',
		autoclose:true
	})
});
